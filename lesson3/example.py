# variable = -1
#
# if variable < 0:
#     abs_variable = -variable
# else:
#     abs_variable = variable
#
# abs_variable = -variable if variable < 0 else variable


# while

# first = 5
# second = 0
#
#
# while first > second:
#     print('Inside while loop')
#     print(f'{first} {second}')
#
#     second += 1
#
# print('Outside loop')


# first = 5
# second = 0
#
#
# while True:
#
#     if first < second:
#         break
#
#     print('Inside while loop')
#     print(f'{first} {second}')
#
#     second += 1
#
#
# print('Outside loop')


# first = 5
# second = 0
#
#
# while True:
#
#     if first < second:
#         break
#     second += 1
#
#     if second % 2 == 0:
#         continue
#
#     print('Inside while loop')
#     print(f'{first} {second}')
#
#
# print('Outside loop')


# first = 5
# second = 0
#
# while True:
#
#     if first < second:
#         break
#
#     second += 1
#
#     if second % 2 == 0:
#         tmp_value = 0
#
#         while tmp_value < second:
#             print('inner loop', tmp_value)
#             tmp_value += 1
#
#         continue
#
#     print('Inside while loop')
#     print(f'{first} {second}')
#
#
# print('Outside loop')

#
# while True:
#     user_input = input('Enter number (int): ')
#
#     if user_input.isdigit():
#         break
#
#     print('Wrong! Only number (int)!')


# list

# my_list = [1, 2.0, True, 'four', None, 1]
#
# print(type(my_list))
#
# print(my_list)
#
# my_list.append(6)
# print(my_list)
#
# my_list.remove(1)
# print(my_list)
#
# print(10 in my_list)
#
# my_list += [1, 2, 3]
#
# print(my_list)
#
# my_list *= 2
#
# print(my_list)


# my_list = [0, 1.2, '2', True, 4, 5, 6]
# print(my_list)
#
# print(my_list[0])
# print(type(my_list[0]))
# print(type(my_list[1]))
# print(type(my_list[2]))
#
# print(my_list[2].endswith('4'))
#
# print(my_list[0:3])
#
# my_list = [0, 1.2, '2', True, [1, 2, 3]]
#
# print(my_list[-1])
# my_list[-1].append(4)
# print(my_list)
#
# my_str = '12345678'
#
# print(my_str[0])
# # my_str[0] = '0'
#
# my_list[0] = [1, 2, 3]
# print(my_list)

# my_list_a = [1, 2, 3]
# my_list_b = [1, 2, 3]
#
# print(my_list_a == my_list_b)
#
# my_list = [0, 1.2, '2', True, [1, 2, 3]]
# print(my_list)
#
# my_list.remove(0)
# print(my_list)
#
# my_list.pop(2)
# print(my_list)
#
#
# my_list = []
#
# my_list.extend([1, 2, 3, 4])  # my_list += [1, 2, 3, 4]
#
#
# print(bool(my_list))
# print(bool([]))
#
# my_str = str([1, 2, 3, 4])
#
# print(len(my_str))
#
# my_list = list('123456789')
# print(my_list)

# my_list = [0, 1.2, '2', True, [1, 2, 3]]
# print(my_list[-1][0])

# my_list = [0, 1.2, '2', True, [1, 2, 3]]

# list_len = len(my_list)
# idx = 0
#
# while idx < list_len:
#     print(my_list[idx])
#
#     idx += 1


# for loop

# my_list = [0, 1.2, '2', True, [1, 2, 3]]


# for i in my_list:
#     print('FOR loop', i)
#     print(type(i))


# my_list = [0, 1.2, '2', True, [1, 2, 3]]
#
# for list_element in my_list:  # str
#     print('FOR loop', list_element)
#     print(type(list_element))

#
# my_list = [0, 1.2, '2', True, [1, 2, 3]]
#
# for list_element in my_list:  # str
#     if isinstance(list_element, str):
#         continue
#     if isinstance(list_element, list):
#         for inner_list_element in list_element:
#             print('---->', inner_list_element)
#
#     print('FOR loop', list_element)
#     print(type(list_element))
#
#     # if isinstance(list_element, bool):
#     #     break
#
# my_new_list = my_list[::3]
#
#
# for list_element in my_list[::3]:
#
#     print('FOR loop', list_element)
#     print(type(list_element))


# # try except
# age = None
#
# try:
#     age = int(input('Enter the number (int): '))
# except:
#     print('Wrong!')
#
# print(age)


# raw_input = input('Enter the number (int): ')
#
# try:
#     age = int(raw_input)
# except:
#     print('Wrong!')


# try:
#     res = 10 / int(input('Enter the divisor (int): ')[0])
# except ZeroDivisionError:
#     print('exception - should not be zero!')
# except ValueError:
#     print('exception - number please!')
# except:
#     print('Exception ---- ')


# try:
#     res = 10 / int(input('Enter the divisor (int): ')[0])
# except (ZeroDivisionError, ValueError):
#     print('exception - should not be zero! or number please')
# except:
#     print('Exception ---- ')


# try:
#     res = 10 / int(input('Enter the divisor (int): ')[0])
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero! or number please')
#     print(type(err))
#     print(err.args)
# except:
#     print('Exception ---- ')


# try:
#     res = 10 / int(input('Enter the divisor (int): '))
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero! or number please')
# else:
#     print('result is:', res)

# try:
#     res = 10 / int(input('Enter the divisor (int): '))
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero! or number please')
# else:
#     print('result is:', res)
# finally:
#     print('Exception or not')


# res = None
#
# try:
#     res = 10 / int(input('Enter the divisor (int): '))
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero! or number please')
#
# print('result is:', res)


# tuple

my_tuple = (0, 1.0, '2', True, 'F', None, [1, (1, 2, 3), 3], )

print(my_tuple)
print(type(my_tuple))

print(my_tuple[4])
print(my_tuple[1:4])

for tuple_element in my_tuple:
    print(tuple_element)

# my_tuple[0] = 10
# my_tuple.append(1)
# my_tuple.pop()

my_tuple += (1, 2)

print(my_tuple)

my_tuple *= 2

print(my_tuple)
print(0 in my_tuple)

print(bool(my_tuple))

my_tuple = tuple()

my_tuple = tuple([1, 2, 3])
print(my_tuple)

my_tuple = tuple('[1, 2, 3]')
print(my_tuple)
