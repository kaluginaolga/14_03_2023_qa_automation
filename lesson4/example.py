# mutable objects

# list1 = [1, 2, 3]
#
# list2 = list1
#
# list2.append(4)
#
# print(list1)
# print(list2)


# a = 1
# b = a
# print(id(a))
# print(id(b))
#
# a += 1
#
# print(id(a))
# print(id(b))
#
# print(a is b)


# list1 = [1, 2, 3]
#
# list2 = list1
#
# list2.append(4)
#
# print(id(list1))
# print(id(list2))
#
# list2 = list1.copy()
#
# list2.append(5)
# print(list1)
# print(list2)
# print(id(list1))
# print(id(list2))
#
#
# list2 = list1[:]
#
# list2.append(5)
# print(list1)
# print(list2)
# print(id(list1))
# print(id(list2))


# list1 = [1, 2, 3, ['a', 'b', 'c']]

# # list2 = list1[:]
# list2 = list1.copy()
#
# list2[-1].append('d')
# list2.append(4)
#
# print(list1)
# print(list2)
# print(id(list1))
# print(id(list2))
# print(id(list1[3]))
# print(id(list2[3]))

# import copy
#
# list1 = [1, 2, 3, ['a', 'b', 'c']]
#
#
# list2 = copy.deepcopy(list1)
#
# list2[-1].append('d')
# list2.append(4)
#
# print(list1)
# print(list2)
# print(id(list1))
# print(id(list2))
# print(id(list1[3]))
# print(id(list2[3]))


# dict, set, frozenset

#
# list1 = [1, 2, 3, ['a', 'b', 'c']]
#
# print(list1[-1])
#
# my_set = {2, 1, 2, (1, 2), 4, 1, 1, 1, 1}
#
# print(my_set)
# print(1 in my_set)
#
# my_set.add(0)
# my_set.add('0')
# my_set.add('a')
#
# print(my_set)
#
# my_set.remove(1)
# print(my_set)
#
# for element in my_set:
#     print(element)
#
# my_set.update({5, 3, 6, 9})
# print(my_set)
#
# my_list = [1, 1, 2, 3, 4]
# my_tuple = (1, 1, 2, 3, 4)
#
# my_set = set('my_tuple')
#
# print(my_set)
#
# my_list = list(my_set)
# print(my_list)
#
# # frozenset
#
# my_frozenset = frozenset(my_list)
#
# print(my_frozenset)


# dict


# my_dict = {}
#
# print(type(my_dict))

# my_dict = {
#     # key: value,
#     0.2: 0,
#     1.0: 1.0,
#     'email': 'sdfghj',
#     'password': 'sdfghj',
#     False: True,
#     (1, 2, 3): [1, 2, 3, 4],
#     None: {1, 2, 3},
#     123: {1: 2, 2: 3, 3: 4}
# }
#
# print(my_dict['email'])
# print(my_dict[0.2])
# print(my_dict[False])
# print(my_dict[(1, 2, 3)])
#
# # my_dict[(1, 2, 3)][5] = 10
#
# print(my_dict)
#
# my_dict['email123'] = 10
# my_dict[None] = 10000
#
# print(my_dict)
#
# my_dict.update({321: 321})
# my_dict.update({None: 321, 1: 2})
#
# print(my_dict)
#
# my_dict.pop(None)
#
# print(my_dict)


# my_dict = {
#     'first': [1, 2, 3],
#     'second': {
#         'a': 'b',
#         'c': 'd',
#     },
#     'third': True,
# }

# print(my_dict)
# print(my_dict['second']['a'])
# print(my_dict['first'][-1])
# print(my_dict == {1: 2})
# print({1: 2} == {1: 2})


# my_dict = {
#     'first': [1, 2, 3],
#     'second': {
#         'a': 'b',
#         'c': 'd',
#     },
#     'third': True,
# }

#
# for i in my_dict:
#     print(i)

# for key in my_dict:
#     print(key)

# for key in my_dict.keys():
#     print(key)
#     print(my_dict[key])

# for value in my_dict.values():
#     print(value)

# for item in my_dict.items():
#     print(type(item))
#     print(f'key is {item[0]}')
#     print(f'value is {item[1]}')

# for key, value in my_dict.items():
#
#     print(f'key is {key}')
#     print(f'value is {value}')
#
#
# print('five' in my_dict)
# print('five' in my_dict.keys())
# print([1, 2, 3] in my_dict.values())


# print(my_dict['five'])
#
# value = my_dict['five']


# my_dict = {
#     'first': [1, 2, 3],
#     'second': {
#         'a': 'b',
#         'c': 'd',
#     },
#     'third': True,
# }
#
# value = None
# if 'five' in my_dict:
#     value = my_dict['five']
#
# print(value)
#
#
# value = my_dict['five'] if 'five' in my_dict else None
#
# print(value)
#
#
# value = my_dict.get('five', [3, 2, 1])  # my_dict['five'] if 'five' in my_dict else [3, 2, 1]
# print(value)
#
#
# print(my_dict)
# value = my_dict.setdefault('five', [3, 2, 1])
# print(my_dict)
# print(value)
#
# print(bool(my_dict))
# print(bool({}))
#
# print(list(my_dict.values()))


# comprehensions

#
# lst = []
# for number in range(1, 11):
#     lst.append(number ** 2)
#
# print(lst)
#
# lst = [number ** 2 for number in range(1, 11)]
# print(lst)
#
# lst = [str(number ** 2 if number % 2 == 0 else number ** 3) for number in range(1, 11)]
# print(lst)
#
# lst = [number ** 2 for number in range(1, 11) if number % 2 == 0]
# print(lst)
#
# my_set = {number ** 2 for number in range(1, 11) if number % 2 == 0}
# print(my_set)
#
# my_generator = (number ** 2 for number in range(1, 11) if number % 2 == 0)
# print(my_generator)
#
# my_dict = {str(number): number ** 2 for number in range(1, 11) if number % 2 == 0}
# print(my_dict)


# positional unpacking

# lst = [1, 2, 3]
#
# first = lst[0]
# second = lst[1]
# third = lst[2]
#
# print(first, second, third)
#
#
# first, second, third = lst
#
# print(first, second, third)


# lst = [1, 2]
#
#
# first, second = lst
#
# print(first, second)
#
#
# dct = {1: 2}
#
# for item in dct.items():  # item = (key, value)
#     print(item)
#
# for k, v in dct.items():  # k, v = (key, value)
#     print(k, v)


# lst = [1, 2, 3, 4]
# lst = [1, 2]
#
#
# first, second, *third = lst
#
# print(first)
# print(second)
# print(third)


# lst = [1, 2, 3]
#
#
# *first, last = lst
#
# print(first)
# print(last)


# lst = [1, 2, 2, 2, 2, 3]
#
#
# first, *avg, last = lst
#
# print(first)
# print(avg)
# print(last)


# first = 1
# second = 2
#
# first, second = second, first
#
# print(first)
# print(second)
#
#
# first = 10
# second = 10
#
# print(first)
# print(second)
#
# first = second = 10
#
# print(first)
# print(second)


# DRY

# KISS

# YAGNI



