# my_str = '123456'
#
# print('abc' in my_str)
#
# res = len(my_str)
# print(res)
#
# user_input = input('Input smth here: ')
#
# print(type(user_input))
# print(user_input)

# str to numbers
#
# my_int = int('12330')
#
# print(type(my_int))
# print(my_int)
#
#
# my_float = float('123.30')
#
# print(type(my_float))
# print(my_float)
#
# my_bool = bool(' ')
# print(my_bool)
#
#
# # formatting
#
# my_str = '1224' + 'jhgcusdgcv'
#
# res = 'My existing ' + str(my_int) + ' str'
# print(res)
#
# res = 'My existing %s str %s' % (my_int, my_float)
# print(res)
#
# res = 'My existing {} str {}'.format(my_int, my_float)
# print(res)
#
# tpl = 'My existing {} str {}'
# res = tpl.format(my_int, my_float)
# print(res)
#
# tpl = 'My existing {first} str {second}'
# res = tpl.format(second=my_int, first=my_float)
# print(res)
#
# res = f'My existing {my_int + 4} str {my_float * 3}'
# print(res)
#
#
# # str methods
#
# tpl = '1234 1234 1234'
# result = tpl.replace('23', 'a')
# print(result)
#
# tpl = '1234 1234 1234'
# result = tpl.replace('23', '')
# print(result)
#
# tpl = '---1234 1234 1234---'
# result = tpl.strip('-')
# print(result)
#
# tpl = '---1234 1234 1234---'
# result = tpl.lstrip('-')
# print(result)
#
# tpl = '---1234 1234 1234---'
# result = tpl.rstrip('-')
# print(result)
#
# tpl = 'abcd 12345 abcd'
# result = tpl.upper()
# print(result)
#
# tpl = 'ABCD 12345 ABCD'
# result = tpl.lower()
# print(result)
#
# tpl = 'abCd 12345 aBcd'
# result = tpl.title()
# print(result)
#
# tpl = 'abCd 12345 aBcd'
# result = tpl.capitalize()
# print(tpl)
# print(result)
#
# tpl = 'abCd 12345 abcd'
# result = tpl.count('ab')
# print(result)
#
# tpl = 'abCd 12345 abcd'
# result = tpl.startswith('abc')
# print(result)
#
# tpl = 'abCd 12345 abcd'
# result = tpl.endswith('bcd')
# print(result)
#
# tpl = '12345'
# result = tpl.isdigit()
# print(result)
#
# tpl = 'jhagscbja'
# result = tpl.isalpha()
# print(result)
#
# tpl = 'jhag s122342cbja'
# result = tpl.isalnum()
# print(result)
#
# tpl = 'abcdefjh'
# result = tpl.find('ab')
# print(result)
#
# my_str = '0123456789'
# # my_str = 'abcdefjihk'
# print(my_str[3])
# # print(my_str[10])
#
# res = my_str[3]
# print(type(res))
#
# last_idx = len(my_str) - 1
# print(my_str[last_idx])
# print(my_str[len(my_str) - 1])
#
# print(my_str[-2])
#
# print(my_str[2:5])
# print(my_str[-5:-1])
#
# print(my_str[3:])
# print(my_str[:6])
# print(my_str[1:9:2])
# print(my_str[-1:-8:-1])
# print(my_str[2::-1])
# print(my_str[::-2])
#
# print(my_str[len(my_str) // 2:])
#
# begin_idx = len(my_str) // 2
#
# print(my_str[begin_idx:])
#
# print(my_str[2:9:2])


# if else


# condition = False
#
# if condition:
#     print('It\'s True!')
#     print('It\'s True!')
#     print('It\'s True!')
#     print('It\'s True!')
# else:
#     print('It\'s False!')
#     print('It\'s False!')
#     print('It\'s False!')
#     print('It\'s False!')
#
#
# print('Outside IF-ELSE')


# first = 100
# second = 20
#
# if first > second:  # bool(condition)
#     print('first > second')
# else:
#     print('first <= second')
#
#
# print('Outside IF-ELSE')


# first = 20
# second = 20
#
# res = first - second
#
# if res:  # bool(res)
#     print('not a zero')
# else:
#     print('zero!')
#
#
# print('Outside IF-ELSE')


# first = 10
# second = 20
#
# if first > second:  # bool(condition)
#     print('first > second')
# elif first == second:
#     print('first == second')
# elif first < 20:
#     print('first < 20')
# else:
#     print('first < second')
#
#
# print('Outside IF-ELSE')


# first = 30
# second = 20
#
# if first > second:  # bool(condition)
#     print('first > second')
#
#
# print('Outside IF-ELSE')


# first = 20
# second = 20
#
# if first > second:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')

# first = 5
# second = 2
#
# if first > second:
#     print('first > second')
#     if first > 10:
#         print('first > 10')
#
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')


# first = 6
# second = 2
#
# if first > second and first > 5 and second > 0:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')


# first = 11
# second = 22
#
# if first > second or first > 10:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')


# res = 1 and 'abcd' and 3 and 2.3
#
# print(res)
#
# res = 0 or 'fvg' or False or 0.0
#
# print(res)


"""
( )                                             Дужки
**	                                            Піднесення до ступеня
*, /, //, %                                     Множення, ділення, цілочисленне ділення, залишок від ділення
+, -	                                        Додавання, віднімання
==, !=, >, >=, <, <=, is, is not, in, not in    Порівняння, перевірка ідентичності, входження
not	                                            Логічне інвертування
and	                                            Логічне "І"
or                                              Логічне "АБО"
=                                               Прирівнювання


"""

# first = 11
# second = 22
#
# if first > second and first > 10 or second > 10 and first < 10:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')

# first = 11
# second = 22
#
# first_option = first > second and first > 10
# second_option = second > 10 and first < 10
#
# if first_option or second_option:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')

# first = 11
# second = 22
#
# if first > second and (first > 10 or second > 10) and first < 10:
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')

# first = 11
# second = '123456'
#
# if not second.isalpha():
#     print('first > second')
# elif first < second:
#     print('first < second')
#
# print('Outside IF-ELSE')

first = 10

# if first == True:  # Bad idea
#     print('first - True')

if first is True:  # if first is None:
    print('first - True')

print('Outside IF-ELSE')

