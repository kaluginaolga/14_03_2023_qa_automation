# a = 10
# b = 20
# c = a + b
# print(c)
#
#
# a = '10'
# c = a + b

#
# variable = 10
# # students_counter = 0
#
# VARIABLE = 20
#
# Variable_1 = 30
# # 1Variable1 = 30
#
# studentsCounterOnLesson = 30
#
# _students_counter = 30
# __students_counter = 30

# O o l I


# Primitive data types

# numbers

# int
# float

# my_int1 = 23
# my_int2 = 7
#
# print(type(my_int2))
#
# result = my_int1 + my_int2
# print(result)
#
# result = my_int1 - my_int2
# print(result)
#
# result = my_int1 * my_int2
# print(result)
#
# result = my_int1 ** my_int2
# print(result)
#
# result = my_int1 / my_int2
# print(result)
#
# result = my_int1 // my_int2
# print(result)
#
# result = (my_int1 % my_int2) + 3  # 23 - 7 * 3
# print(result)
#
#
# my_int1 += my_int2  # my_int1 = my_int1 + my_int2
# print(my_int1)
#
# my_int1 /= my_int2  # my_int1 = my_int1 / my_int2
# print(my_int1)
#
# big_value = 1000 ** 1000
#
#
# # float
#
# my_float1 = 1.5
# my_float2 = 1.5
#
#
# res = my_float1 + my_float2
# print(res)
#
# res = my_int1 + my_float2
# print(res)
# print(type(res))

# my_float1 = 0.00000000000000004
# print(my_float1)
#
# my_float1 = 4e-17  # 4 * 10 ** -17
# print(my_float1)

# my_float1 = 0.1
# my_float2 = 0.2
#
# res = my_float1 + my_float2
# print(res)
# print(round(0.1234567, 3))

# bool
#
# my_float1 = 0.1
# my_float2 = 0.2
#
# res = my_float1 + my_float2
#
# compare_res = res < 0.3
# print(compare_res)
#
# compare_res = res > 0.3
# print(compare_res)
#
# # compare_res = res <= 0.3
# # compare_res = res >= 0.3
# # compare_res = res == 0.3
# # compare_res = res != 0.3
#
# compare_res = res == 0.3
# print(compare_res)
#
# my_bool = True
#
# # my_bool = False
#
# print(type(my_bool))
#
#
# # None
#
# my_none = None
#
#
# # to int
#
# my_float = 1.234
# my_int = int(my_float)
# print(my_int)
#
# my_int = int(True)
# print(my_int)
#
# my_int = int(False)
# print(my_int)
#
# # to float
#
# my_int = 10
#
# my_float = float(my_int)
# print(my_float)
#
#
# my_float = float(True)
# print(my_float)
#
#
# # my_float = float(None)
# # print(my_float)
#
# # to bool
#
# my_bool = bool(-10)
# print(my_bool)
#
# my_bool = bool(0)
# print(my_bool)
#
# my_bool = bool(None)
# print(my_bool)


# str

# my_str = 'awecvdfbg1234245sdvdb'
# print(my_str)
# print(type(my_str))
#
# my_str = "12345''' ' ' 678"
#
# my_str = '12345 """ 678'


# my_str = "12345 ' 678"
# print(my_str)
#
# my_str = '12345 \' 678'
# print(my_str)
#
# my_str = '12345 \n 678'
# print(my_str)
#
# my_str = '''ajjgvcjnfvg
#  jsbdvikl
#  jasbdkvl f
#  vjadb kvl
#  jva sc
#  '''
# print(my_str)
#
# my_str = """ajjgvcjnfvg
#  jsbdvikl
#  jasbdkvl f
#  vjadb kvl
#  jva sc
#  """
# print(my_str)

#
# my_str1 = '12345678'
# my_str2 = 'abcdef'
#
# res = my_str1 + my_str2 + '!@#$%' + """ ''' """
#
# print(res)
#
# res = '(my string)' * 10
# print(res)
#
# my_int = 10.0
#
# my_str2 = str(my_int)
#
# res = my_str1 + my_str2
# print(res)

# task1. hbkdfv nkdfl


# task2. hbkdfv nkdfl
