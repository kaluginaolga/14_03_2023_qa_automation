# Functions

# def function_name(arg1, arg2):
#     print('inside function_name arg1 is:', arg1)
#     print('inside function_name arg2 is:', arg2)
#     res = arg1 + arg2
#     return res
#
#
# result = function_name(1, 2)
# print('result is', result)
#
# result = function_name(2, 2)
# print('result is', result)


# def custom_add(first, second):
#     print('inside function_name first is:', first)
#     print('inside function_name second is:', second)
#     res = first + second
#     return res
#
#
# result = custom_add(1, 2)
# print('result is', result)


# def custom_add():
#     print('inside function_name first is:')
#     return
#
#
# result = custom_add()

# first = '1234'
#
#
# def custom_add(first, second):
#     to_print(second)
#     print('inside function_name first is:', first)
#     print('inside function_name second is:', second)
#     res = first + second
#
#     return res
#
#
# def to_print(first):  # bad idea
#     print(first)
#
#
# res = custom_add(1, 2)


# def custom_add(first, second):
#     print('inside function_name first is:', first)
#     print('inside function_name second is:', second)
#     res = first + second
#
#     return res
#
#
# result = custom_add(6, 5)  # positional
# print(result)
#
# result = custom_add(first=6, second=5)  # named
# print(result)
#
# result = custom_add(
#     second=5,
#     first=6,
# )
#
# print(result)
#
# result = custom_add(
#     second=5,
#     first=6,
# )


# def custom_function(a, b, c, d):
#     print(f'a is {a}')
#     print(f'b is {b}')
#     print(f'c is {c}')
#     print(f'd is {d}')
#
#
# # custom_function(1, 2, 3, 4)
# # custom_function(d=1, c=2, a=3, b=4)
#
# # custom_function(1, 2, c=10, d=20)
#
# result = custom_function(1, 2, c=10, d=20)
#
# print(result)


# def custom_function(a=0, b=0, c=0, d=0):
#     if a == b == c == d == 0:
#         return
#     print(f'a is {a}')
#     print(f'b is {b}')
#     print(f'c is {c}')
#     print(f'd is {d}')


# custom_function(1, 2, 3, 4)
# custom_function()


# def custom_function(a=0, b=0, c=0, d=None):
#     if d is None:
#         return
#     print(f'a is {a}')
#     print(f'b is {b}')
#     print(f'c is {c}')
#     print(f'd is {d}')
#
#
# custom_function(d=4)


# def custom_function(a=0, b=0, c=0, d=None):
#     if d is None:
#         return [1, 3]
#
#     print(f'a is {a}')
#     print(f'b is {b}')
#
#     if a + b == 0:
#         return 2, 3
#
#     print(f'c is {c}')
#     print(f'd is {d}')
#
#
# custom_function(c='sdfg', d=4)
#
# # my_tuple = 1, 2, 3


# def add_to_list(value, list_to_add=[]):
#
#     list_to_add.append(value)
#
#     return list_to_add
#
#
# my_list = [1, 2, 3]
#
# my_list = add_to_list(4, my_list)
# print(my_list)
#
# my_list = add_to_list(5, my_list)
# print(my_list)
#
# my_new_list = add_to_list(1)
# print(my_new_list)
#
# my_list = add_to_list(6, my_list)
# print(my_list)
#
# my_new_list = add_to_list('1')
# print(my_new_list)
#
# my_new_list = add_to_list(True)
# print(my_new_list)


# def add_to_list(value, list_to_add=None):
#
#     if list_to_add is None:
#         list_to_add = []
#
#     list_to_add.append(value)
#
#     return list_to_add
#
#
# my_new_list = add_to_list(True)
# print(my_new_list)
# my_new_list = add_to_list(True)
# print(my_new_list)


# def my_function(my_list):
#     my_list.append(1)
#     return my_list
#
#
# my_list = [1, 2, 3]
#
# print(my_list)
#
# my_list = my_function(my_list)
#
# print(my_list)


# def custom_function(a, b, c, d):
#     print(f'a is {a}, {type(a)}')
#     print(f'b is {b}, {type(b)}')
#     print(f'c is {c}, {type(c)}')
#     print(f'd is {d}, {type(d)}')
#     return
#
#
# custom_function(1, '2', True, None)


# def custom_function(*args):  # *args - positional
#     print(f'args is {args}, {type(args)}')
#
#     return
#
#
# custom_function()
# custom_function(1, '2')
# custom_function(1, '2', True, None)
#
# my_list = [1, 2, 3, 4]
#
# # custom_function(my_list[0], my_list[2])
# custom_function(*my_list)  # custom_function(my_list[0], my_list[2], ....)

# my_list = [1, 2, 3, 4]
#
#
# def custom_function(a, b=0, *args):  # *args - positional
#     print(a)
#     print(b)
#     print(f'args is {args}, {type(args)}')
#     return
#
#
# custom_function(1, 2, 3, 4, 5, 5)
# custom_function(1, *my_list)


# my_list = [1, 2, 3, 4]
#
#
# def custom_function(a, b=20, **kwargs):  # **kwargs - named
#
#     print(f'kwargs is {kwargs}, {type(kwargs)}')
#     return
#
#
# custom_function(10, b=20, c=30)

#
# my_list = [1, 2, 3, 4]
#
# my_dict = {
#     'm': 10,
#     'k': 30,
# }
#
#
# def custom_function(a, b, c=10, d=10, *args, **kwargs):
#     print(a, b, c, d)
#     print(f'args is {args}, {type(args)}')
#     print(f'kwargs is {kwargs}, {type(kwargs)}')
#     return
#
#
# custom_function(10, 20, 30, *my_list, **my_dict)  # m=10, k=30


# my_dict = {
#     'a': 10,
#     'b': 10,
#     'c': 10,
#     'd': 10,
#     'm': 10,
#     'k': 30,
# }
#
#
# def custom_function(a, b, c=10, d=10, *args, **kwargs):
#     print(a, b, c, d)
#     print(f'args is {args}, {type(args)}')
#     print(f'kwargs is {kwargs}, {type(kwargs)}')
#     return
#
#
# custom_function(**my_dict)


# game

    # AI -> number

    # User get number from keyboard

    # User == AI


from random import randint


def get_ai_number():
    number = randint(1, 10)
    print(f'AI: {number}')
    return number


def get_user_number():

    while True:
        try:
            return int(input('Enter the number (int): '))
        except ValueError:
            print('Number please!')


def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    print(f'Result is: {result}')
    return result


def game_guess_number():
    print('Game begins!')

    ai_number = get_ai_number()

    while True:
        user_number = get_user_number()
        is_game_end = check_numbers(ai_number, user_number)

        if is_game_end:
            break
        print('Wrong, try again!')

    print('User win')


game_guess_number()
