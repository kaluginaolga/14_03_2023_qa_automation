# Functions

# def my_function(arg1, arg2=10, *args, **kwargs):
#     print(arg1, arg2, args, kwargs)
#     return
#
#
# my_function(arg1=1, arg2=2, a=3, b=4)


# def my_function(value):
#     print(value)
#
#
# my_function(1)

# print(dir())
#
# value = 1098748
#
# PI = 3.14
#
#
# def foo():
#     my_function()
#
#
# foo()
#
#
# def my_function():
#     value = 10
#     print(value)
#     print(value + PI)
#     value = str(value)
#
#
# my_function()

#
# value = 1098748
#
#
# def my_function(value):
#
#     print(value)
#     value = str(value)
#     return value
#
#
# value = my_function(value)


# my_list = []
#
#
# def my_function():
#     # my_list += []
#     my_list.append(10)
#
#
# print(my_list)
#
# my_function()
#
# print(my_list)


# my_list = []
#
#
# def my_function(my_list):
#     # my_list += []
#     my_list.append(10)
#     return my_list
#
#
# print(my_list)
#
# my_list = my_function(my_list)
#
# print(my_list)


# global_value = 100
#
#
# def my_function():
#     global global_value
#
#     global_value += 10
#     global_value = list(str(global_value))
#
#
# print(global_value)
#
# my_function()
#
# print(global_value)


# def my_add_function(arg1, arg2):
#     """
#     Docstring function for add a to b
#     Bla bla bla
#     """
#     return arg1 + arg2


# def my_add_function(arg1, arg2):
#     """
#     Docstring function for add a to b
#     Bla bla bla
#
#     :param arg1: first
#     :type arg1: int
#     :param arg2: second
#     :type arg2: float
#
#     :return:
#     :rtype: float
#
#     """
#     return arg1 + arg2
#
#
# res = my_add_function(1, 1.2)
#
# res = my_add_function('1.2', '1.2')
#
# res = my_add_function(res, 1.2)


# def my_add_function(arg1, arg2):
#     """
#     Docstring function for add a to b
#     Bla bla bla
#
#     Args:
#         arg1 (int):
#         arg2 (float):
#     Returns:
#         float:
#     """
#     return arg1 + arg2
#
#
# res = my_add_function(1, 1.2)


# def my_add_function(arg1: int = 10, arg2: float = 20) -> float:  # Annotation
#     """
#     Docstring function for add a to b
#     Bla bla bla
#     """
#     # bla bla
#     return arg1 + arg2
#
#
# res = my_add_function('1', '1.2')


# useful functions

# int
# float
# str
# bool


# my_int = int('123')
# print(my_int)
#
# my_int = int(1.3)
# print(my_int)
#
# # list
# # tuple
# # set
# # frozenset
#
# my_list = list(range(1, 10))
# print(my_list)
#
# # dict
#
# my_dict = dict()
# print(my_dict)
#
# my_dict = dict(a=10, b=20)  # key -> str
# print(my_dict)
#
# my_dict = dict([(1, 2), (3, 4)])
# print(my_dict)
#
#
# # math
#
# val1 = 1
# val2 = 2
#
# res = sum([1, 2, 3, 4, 5, 5])
# print(res)
#
# res = max(val1, val2, 3)
# print(res)
#
# res = max([10, val1, val2, 3])
# print(res)
#
# res = min([10, val1, val2, 3])
# print(res)
#
#
# def foo(val):
#     return val % 3
#
#
# res = max([10, 1, 14, 3], key=foo)
# print(res)
#
# res = max([10, 1, 14, 3], key=lambda val: val % 3)
# print(res)
#
#
# # iteration
#
# rng = range(10, 1, -2)
#
# print(list(rng))
#
#
# enum = enumerate(set('LoremIpsum'))
#
# print(list(enum))


# map

# def foo(val):
#     return str(val % 3) + 'a'
#
#
# data = (3, 4, 5)
#
#
# for i in data:
#     res = foo(i)
#     print(res)
#
# mapped = map(foo, data)
#
# for i in mapped:
#     print(i)
#
#
# for i in map(foo, data):
#     print(i)

#
# data1 = (1, 2, 3, 4)
# data2 = {'a', 'b', 'c'}
# data3 = 'lorem ipsum'
#
# # zipped = zip(data1, data2, data3)
# zipped = zip(data1, data2)
#
# for i in zipped:
#     print(i)
#
# my_dict = dict(zip(data1, data2))
# print(my_dict)


# data = 'lorem ipsum'
#
# rev = reversed(data)
#
# print(list(rev))


# def foo(val):
#     return val % 3
#
#
# data = [3, 5, 1, 88, 22, 7, 54, 3, 1]
#
# res = sorted(data, reverse=True, key=foo)
#
# print(res)


# def foo(val):
#     return val % 3 != 0
#
#
# data = [3, 5, 1, 88, 22, 7, 54, 3, 1]
#
#
# filtered = list(filter(foo, data))
#
# print(filtered)

# a = 10
# b = 20
#
#
# if a > b and a > 0 and b > 0 and a + b < 100:
#     print('Match!')
#
#
# all_conditions = (
#     (a > b),
#     (a > 0),
#     (b > 0),
#     (a + b < 100),
# )
#
# if all(all_conditions):
#     print('Match!')
#
# if any(all_conditions):
#     print('any Match!')


def foo(val):
    return val % 3


res = max([10, 1, 14, 3], key=foo)
print(res)

res = max([10, 1, 14, 3], key=lambda val: val % 3)
print(res)


my_lambda = lambda val: val % 3  # BAD IDEA!

res = max([10, 1, 14, 3], key=my_lambda)
print(res)

# lambda *args, **kwargs: some action with args kwargs


res = max(['10', '1', '14', '3'], key=lambda val: int(val))  # bad idea!
print(res)

res = max(['10', '1', '14', '3'], key=int)
print(res)

res = max(['10', '1', '14', '3'], key=lambda val: int(val) / 3)
print(res)

res = max(['A10', 'O', '14d', 'a'])
print(res)
